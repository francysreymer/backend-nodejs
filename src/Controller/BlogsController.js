const Sequelize = require('sequelize');
const CompanyModel = require('../Model/Company');
const CategoryModel = require('../Model/Category');
const CategoriesController = require('./CategoriesController');
const cloneDeep = require('clone-deep');

class BlogsController {
  constructor(model, sequelize) {
    this._db = model;
    this._connection = sequelize;
  }

  static async defineModel(Blog) {
    const {Company} = await CompanyModel(Sequelize)
    Blog.belongsTo(Company, {foreignKey : 'company_id', as: 'company'});
    return Blog
  }

  async isConnected() {
    try {
      await this._connection.authenticate();
      return true;
    } catch (error) {
      console.error('fail!', error);
      return false;
    }
  }

  async create(item) {
    try {
      return await this._db.create(item);
    } catch (error) {
      console.error('fail create: ', error);
      return false;
    }
  }

  async read(item) {
    try {

      item = {...item, active: true}
      let blogs = await this._db.findAll(
        {
          where: item,
          order: [
            ['updated_at', 'DESC'],
          ],
          include: [{ all: true, nested: true }],
        }
      );

      if(item.id) {
        const {Category, sequelize} = await CategoryModel(Sequelize)
        const model = await CategoriesController.defineModel(Category)
        const categoryObj = new CategoriesController(model, sequelize);

        blogs[0].dataValues.categoriesData = []
        if(blogs[0].dataValues.categories.length) {
          let categories = await categoryObj.read({id: blogs[0].dataValues.categories});
          categories.map((obj) => {
            blogs[0].dataValues.categoriesData.push({id: obj.id, name: obj.name})
          })
        } else {
          blogs[0].dataValues.categories = []
        }
      }

      return blogs
    } catch (error) {
      console.error('fail read: ', error);
      return false;
    }
  }

  async update(id, item) {
    try {
      return await this._db.update(
        item,
        {
          where: {
            id: id,
            active: true
          }
      });
    } catch (error) {
      console.error('fail update: ', error);
      return false;
    }
  }

  async delete(id) {
    try {
      return await this._db.update(
        {active: false},
        {
          where: {
            id: id,
            active: true
          }
      });
    } catch (error) {
      console.error('fail delete: ', error);
      return false;
    }
  }

}

module.exports = BlogsController