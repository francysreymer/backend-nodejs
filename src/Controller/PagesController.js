const Sequelize = require('sequelize');
const BlockModel = require('../Model/Block');
const BlocksController = require('./BlocksController');

class PagesController {
  constructor(model, sequelize) {
    this._db = model;
    this._connection = sequelize;
  }

  static async defineModel(Page) {
    const {Block} = await BlockModel(Sequelize)
    Page.hasMany(Block, { foreignKey: 'page_id', as: 'blocks' })
    return Page
  }

  async isConnected() {
    try {
      await this._connection.authenticate();
      return true;
    } catch (error) {
      console.error('fail!', error);
      return false;
    }
  }

  async create(item) {
    try {
      return await this._db.create(item);
    } catch (error) {
      console.error('fail create: ', error);
      return false;
    }
  }

  async read(item) {
    try {

      item = {...item, active: true}
      let pages = await this._db.findAll(
        {
          where: item,
          order: [
            ['position', 'ASC'],
          ],
          include: [{ all: true, nested: true, where: {active: true}, required: false }],
        }
      );

      return pages
    } catch (error) {
      console.error('fail read: ', error);
      return false;
    }
  }

  async update(id, item) {
    try {
      return await this._db.update(
        item,
        {
          where: {
            id: id,
            active: true
          }
      });
    } catch (error) {
      console.error('fail update: ', error);
      return false;
    }
  }

  async delete(id) {
    try {
      return await this._db.update(
        {active: false},
        {
          where: {
            id: id,
            active: true
          }
      });
    } catch (error) {
      console.error('fail delete: ', error);
      return false;
    }
  }

}

module.exports = PagesController