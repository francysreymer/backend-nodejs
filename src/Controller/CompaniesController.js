const Sequelize = require('sequelize');
const SiteModel = require('../Model/Site');

class CompaniesController {
  constructor(model, sequelize) {
    this._db = model;
    this._connection = sequelize;
  }

  static async defineModel(Company) {
    const {Site} = await SiteModel(Sequelize)
    Company.hasMany(Site, {foreignKey : 'company_id', as: 'sites'});
    return Company
  }

  async isConnected() {
    try {
      await this._connection.authenticate();
      return true;
    } catch (error) {
      console.error('fail!', error);
      return false;
    }
  }

  async create(item) {
    try {
      return await this._db.create(item);
    } catch (error) {
      console.error('fail create: ', error);
      return false;
    }
  }

  async read(item) {
    try {
      const {Site} = await SiteModel(Sequelize)

      item = {...item, active: true}
      return await this._db.findAll({
        where: item, 
        order: [
          ['name', 'ASC'],
        ],
        include: [
          { 
            model: Site, 
            as: 'sites', 
            required: false
          }
        ] 
      });
    } catch (error) {
      console.error('fail read: ', error);
      return false;
    }
  }

  async update(id, item) {
    try {
      return await this._db.update(
        item,
        {
          where: {
            id: id,
            active: true
          },
      });
    } catch (error) {
      console.error('fail update: ', error);
      return false;
    }
  }

  async delete(id) {
    try {
      return await this._db.update(
        {active: false},
        {
          where: {
            id: id,
            active: true
          }
      });
    } catch (error) {
      console.error('fail delete: ', error);
      return false;
    }
  }

}

module.exports = CompaniesController;