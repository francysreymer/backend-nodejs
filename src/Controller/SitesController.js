const Sequelize = require('sequelize');
const CompanyModel = require('../Model/Company');
const ThemeModel = require('../Model/Theme');

class SitesController {
  constructor(model, sequelize) {
    this._db = model;
    this._connection = sequelize;
  }

  static async defineModel(Site) {
    const {Company} = await CompanyModel(Sequelize)
    const {Theme} = await ThemeModel(Sequelize)
    Site.belongsTo(Company, {foreignKey : 'company_id', as: 'company'});
    Site.belongsTo(Theme, {foreignKey : 'theme_id', as: 'theme'});
    return Site
  }

  async isConnected() {
    try {
      await this._connection.authenticate();
      return true;
    } catch (error) {
      console.error('fail!', error);
      return false;
    }
  }

  async create(item) {
    try {
      return await this._db.create(item);
    } catch (error) {
      console.error('fail create: ', error);
      return false;
    }
  }

  async read(item) {
    try {
      const {Company} = await CompanyModel(Sequelize)

      item = {...item, active: true}
      return await this._db.findAll(
        {
          where: item,
          include: [{ all: true, nested: true }],
        }
      );
    } catch (error) {
      console.error('fail read: ', error);
      return false;
    }
  }

  async update(id, item) {
    try {
      return await this._db.update(
        item,
        {
          where: {
            id: id,
            active: true
          }
      });
    } catch (error) {
      console.error('fail update: ', error);
      return false;
    }
  }

  async delete(id) {
    try {
      return await this._db.update(
        {active: false},
        {
          where: {
            id: id,
            active: true
          }
      });
    } catch (error) {
      console.error('fail delete: ', error);
      return false;
    }
  }

}

module.exports = SitesController