const PageModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const Page = sequelize.define('page', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      url: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },   
      position: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      site_id: {
        type: Sequelize.INTEGER,
        required: true,
        allowNull: false,
        references: {
          // This is a reference to another model
          model: 'sites',
          // This is the column name of the referenced model
          key: 'id',
        }
      } 
    }, 
    { 
      tableName: 'pages',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }
  )

  await Page.sync()

  return {Page, sequelize}
}

module.exports = PageModel