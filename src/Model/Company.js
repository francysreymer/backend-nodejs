const CompanyModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const Company = sequelize.define('company', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      document: {
        type: Sequelize.STRING,
        allowNull: true
      },   
      responsible: {
        type: Sequelize.STRING,
        allowNull: true
      },      
      responsible_email: {
        type: Sequelize.STRING,
        allowNull: true
      },      
      responsible_phone: {
        type: Sequelize.STRING,
        allowNull: true
      },  
      zip_code: {
        type: Sequelize.STRING,
        allowNull: true
      },  
      address: {
        type: Sequelize.STRING,
        allowNull: true
      },  
      number: {
        type: Sequelize.STRING,
        allowNull: true
      },  
      complement: {
        type: Sequelize.STRING,
        allowNull: true
      },  
      neighborhood: {
        type: Sequelize.STRING,
        allowNull: true
      },  
      city: {
        type: Sequelize.STRING,
        allowNull: true
      },  
      state: {
        type: Sequelize.STRING,
        allowNull: true
      }, 
      observation: {
        type: Sequelize.TEXT,
        allowNull: true
      }, 
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }, 
    }, 
    { 
      tableName: 'companies',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }
  )

  await Company.sync()

  return {Company, sequelize};
}

module.exports = CompanyModel