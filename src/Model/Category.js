const CategoryModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const Category = sequelize.define('category', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },  
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      company_id: {
        type: Sequelize.INTEGER,
        references: {
          // This is a reference to another model
          model: 'companies',
          // This is the column name of the referenced model
          key: 'id',
        }
      } 
    }, 
    { 
      tableName: 'categories',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }
  )

  await Category.sync()

  return {Category, sequelize}
}

module.exports = CategoryModel