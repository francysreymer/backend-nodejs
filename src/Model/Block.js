const BlockModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const Block = sequelize.define('block', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      content: {
        type: Sequelize.JSON,
        required: true,
        allowNull: false
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      page_id: {
        type: Sequelize.INTEGER,
        required: true,
        allowNull: false,
        references: {
          // This is a reference to another model
          model: 'pages',
          // This is the column name of the referenced model
          key: 'id',
        }
      } 
    }, 
    { 
      tableName: 'blocks',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }
  )

  await Block.sync()

  return {Block, sequelize}
}

module.exports = BlockModel