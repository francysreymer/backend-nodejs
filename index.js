require("dotenv-safe").config();
const jwt = require('jsonwebtoken');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const CompanyModel = require('./src/Model/Company');
const CompaniesController = require('./src/Controller/CompaniesController');
const ThemeModel = require('./src/Model/Theme');
const ThemesController = require('./src/Controller/ThemesController');
const SiteModel = require('./src/Model/Site');
const SitesController = require('./src/Controller/SitesController');
const CategoryModel = require('./src/Model/Category');
const CategoriesController = require('./src/Controller/CategoriesController');
const BlogModel = require('./src/Model/Blog');
const BlogsController = require('./src/Controller/BlogsController');
const PageModel = require('./src/Model/Page');
const PagesController = require('./src/Controller/PagesController');
const BlockModel = require('./src/Model/Block');
const BlocksController = require('./src/Controller/BlocksController');
const UserModel = require('./src/Model/User');
const UsersController = require('./src/Controller/UsersController');

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const verifyJWT = (req, res, next) => {

    const token = req.headers['x-access-token'];
	if (!token) 
		return res.status(401).json({ auth: false, message: 'Nenhum token foi fornecido.' });
    
    jwt.verify(token, process.env.SECRET, function(err, decoded) {
	  if (err) 
	  	return res.status(500).json({ auth: false, message: 'Falha ao autenticar token.' });
      
      req.userId = decoded.id;
      next();
    });
}

app.get('/companies', verifyJWT, async (req, res) => {

	const {Company, sequelize} = await CompanyModel(Sequelize)
	const model = await CompaniesController.defineModel(Company)
	context = new CompaniesController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/companies/add', verifyJWT, async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {Company, sequelize} = await CompanyModel(Sequelize)
	const model = await CompaniesController.defineModel(Company)
	context = new CompaniesController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/companies/read/:id?', verifyJWT, async (req, res) => {
	const id = req.params.id;

	const {Company, sequelize} = await CompanyModel(Sequelize)
	const model = await CompaniesController.defineModel(Company)
	context = new CompaniesController(model, sequelize);

	let result = []
	if (!id) {
		result = await context.read();
	} else {
		result = await context.read({id: id});
	}
	
  	return res.json({ result });
});

app.put('/companies/edit/:id', verifyJWT, async (req, res) => {
	const data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	const {Company, sequelize} = await CompanyModel(Sequelize)
	const model = await CompaniesController.defineModel(Company)
	context = new CompaniesController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/companies/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Company, sequelize} = await CompanyModel(Sequelize)
	const model = await CompaniesController.defineModel(Company)
	context = new CompaniesController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.get('/themes', verifyJWT, async (req, res) => {

	const {Theme, sequelize} = await ThemeModel(Sequelize)
	const model = await ThemesController.defineModel(Theme)
	context = new ThemesController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/themes/add', verifyJWT, async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {Theme, sequelize} = await ThemeModel(Sequelize)
	const model = await ThemesController.defineModel(Theme)
	context = new ThemesController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/themes/read/:id?', verifyJWT, async (req, res) => {
	const id = req.params.id;

	const {Theme, sequelize} = await ThemeModel(Sequelize)
	const model = await ThemesController.defineModel(Theme)
	context = new ThemesController(model, sequelize);

	let result = []
	if (!id) {
		result = await context.read();
	} else {
		result = await context.read({id: id});
	}

  	return res.json({ result });
});

app.put('/themes/edit/:id', verifyJWT, async (req, res) => {
	const data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	const {Theme, sequelize} = await ThemeModel(Sequelize)
	const model = await ThemesController.defineModel(Theme)
	context = new ThemesController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/themes/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Theme, sequelize} = await ThemeModel(Sequelize)
	const model = await ThemesController.defineModel(Theme)
	context = new ThemesController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.get('/sites', verifyJWT, async (req, res) => {

	const {Site, sequelize} = await SiteModel(Sequelize)
	const model = await SitesController.defineModel(Site)
	context = new SitesController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/sites/add', verifyJWT, async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {Site, sequelize} = await SiteModel(Sequelize)
	const model = await SitesController.defineModel(Site)
	context = new SitesController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/sites/read/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Site, sequelize} = await SiteModel(Sequelize)
	const model = await SitesController.defineModel(Site)
	context = new SitesController(model, sequelize);

	const result = await context.read({id: id});

  	return res.json({ result });
});

app.put('/sites/edit/:id', verifyJWT, async (req, res) => {
	const data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	const {Site, sequelize} = await SiteModel(Sequelize)
	const model = await SitesController.defineModel(Site)
	context = new SitesController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/sites/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Site, sequelize} = await SiteModel(Sequelize)
	const model = await SitesController.defineModel(Site)
	context = new SitesController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.get('/categories', verifyJWT, async (req, res) => {

	const {Category, sequelize} = await CategoryModel(Sequelize)
	const model = await CategoriesController.defineModel(Category)
	context = new CategoriesController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/categories/add', verifyJWT, async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {Category, sequelize} = await CategoryModel(Sequelize)
	const model = await CategoriesController.defineModel(Category)
	context = new CategoriesController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/categories/read/:id?', verifyJWT, async (req, res) => {
	const id = req.params.id;

	const {Category, sequelize} = await CategoryModel(Sequelize)
	const model = await CategoriesController.defineModel(Category)
	context = new CategoriesController(model, sequelize);

	let result = []
	if (!id) {
		result = await context.read();
	} else {
		result = await context.read({id: id});
	}

  	return res.json({ result });
});

app.put('/categories/edit/:id', verifyJWT, async (req, res) => {
	const data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	const {Category, sequelize} = await CategoryModel(Sequelize)
	const model = await CategoriesController.defineModel(Category)
	context = new CategoriesController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/categories/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Category, sequelize} = await CategoryModel(Sequelize)
	const model = await CategoriesController.defineModel(Category)
	context = new CategoriesController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.get('/blogs', verifyJWT, async (req, res) => {

	const {Blog, sequelize} = await BlogModel(Sequelize)
	const model = await BlogsController.defineModel(Blog)
	context = new BlogsController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/blogs/add', verifyJWT, async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {Blog, sequelize} = await BlogModel(Sequelize)
	const model = await BlogsController.defineModel(Blog)
	context = new BlogsController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/blogs/read/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Blog, sequelize} = await BlogModel(Sequelize)
	const model = await BlogsController.defineModel(Blog)
	context = new BlogsController(model, sequelize);

	const result = await context.read({id: id});

  	return res.json({ result });
});

app.put('/blogs/edit/:id', verifyJWT, async (req, res) => {
	const data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	const {Blog, sequelize} = await BlogModel(Sequelize)
	const model = await BlogsController.defineModel(Blog)
	context = new BlogsController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/blogs/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Blog, sequelize} = await BlogModel(Sequelize)
	const model = await BlogsController.defineModel(Blog)
	context = new BlogsController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.get('/pages', verifyJWT, async (req, res) => {

	const {Page, sequelize} = await PageModel(Sequelize)
	const model = await PagesController.defineModel(Page)
	context = new PagesController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/pages/add', verifyJWT, async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {Page, sequelize} = await PageModel(Sequelize)
	const model = await PagesController.defineModel(Page)
	context = new PagesController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/pages/read/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Page, sequelize} = await PageModel(Sequelize)
	const model = await PagesController.defineModel(Page)
	context = new PagesController(model, sequelize);

	const result = await context.read({id: id});

  	return res.json({ result });
});

app.put('/pages/edit/:id', verifyJWT, async (req, res) => {
	const data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	const {Page, sequelize} = await PageModel(Sequelize)
	const model = await PagesController.defineModel(Page)
	context = new PagesController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/pages/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Page, sequelize} = await PageModel(Sequelize)
	const model = await PagesController.defineModel(Page)
	context = new PagesController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.get('/blocks', verifyJWT, async (req, res) => {

	const {Block, sequelize} = await BlockModel(Sequelize)
	const model = await BlocksController.defineModel(Block)
	context = new BlocksController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/blocks/add', verifyJWT, async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {Block, sequelize} = await BlockModel(Sequelize)
	const model = await BlocksController.defineModel(Block)
	context = new BlocksController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/blocks/read/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Block, sequelize} = await BlockModel(Sequelize)
	const model = await BlocksController.defineModel(Block)
	context = new BlocksController(model, sequelize);

	const result = await context.read({id: id});

  	return res.json({ result });
});

app.put('/blocks/edit/:id', verifyJWT, async (req, res) => {
	const data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	const {Block, sequelize} = await BlockModel(Sequelize)
	const model = await BlocksController.defineModel(Block)
	context = new BlocksController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/blocks/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {Block, sequelize} = await BlockModel(Sequelize)
	const model = await BlocksController.defineModel(Block)
	context = new BlocksController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.get('/users', verifyJWT, async (req, res) => {

	const {User, sequelize} = await UserModel(Sequelize)
	const model = await UsersController.defineModel(User)
	context = new UsersController(model, sequelize);

	const data = await context.read(null);

  	return res.json({ data });
});

app.post('/users/add', verifyJWT, async (req, res) => {
	let data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	data.password = await bcrypt.hash(data.password, 10);

	const {User, sequelize} = await UserModel(Sequelize)
	const model = await UsersController.defineModel(User)
	context = new UsersController(model, sequelize);

	const result = await context.create(data);

  	return res.json({ result });
});

app.get('/users/read/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {User, sequelize} = await UserModel(Sequelize)
	const model = await UsersController.defineModel(User)
	context = new UsersController(model, sequelize);

	const result = await context.read({id: id});

  	return res.json({ result });
});

app.put('/users/edit/:id', verifyJWT, async (req, res) => {
	let data = req.body;
	const id = req.params.id;

	if (!data || !id) {
	  return res.status(400).end();
	}

	data.password = await bcrypt.hash(data.password, 10);

	const {User, sequelize} = await UserModel(Sequelize)
	const model = await UsersController.defineModel(User)
	context = new UsersController(model, sequelize);

	const result = await context.update(id, data);

  	return res.json({ result });
});

app.delete('/users/delete/:id', verifyJWT, async (req, res) => {
	const id = req.params.id;

	if (!id) {
	  return res.status(400).end();
	}

	const {User, sequelize} = await UserModel(Sequelize)
	const model = await UsersController.defineModel(User)
	context = new UsersController(model, sequelize);

	const result = await context.delete(id);

  	return res.json({ result });
});

app.post('/users/login', async (req, res) => {
	const data = req.body;

	if (!data) {
	  return res.status(400).end();
	}

	const {User, sequelize} = await UserModel(Sequelize)
	const model = await UsersController.defineModel(User)
	context = new UsersController(model, sequelize);

	const result = await context.login({username: data.username});

	let match = false;
	if(result[0]) {
		match = await bcrypt.compare(data.password, result[0].dataValues.password);
	}

	if(match) {
		const userId = result[0].dataValues.id;
		const token = jwt.sign({ userId }, process.env.SECRET, {
			expiresIn: 5000 // expires in 5min
		});
		return res.json({ auth: true, token: token, user: result[0].dataValues });
	} else {
		res.status(500).json({message: 'Login e/ou senha inválido(s)!'});
	}
});

app.listen(9000, () => console.log('Express started at http://localhost:9000'));