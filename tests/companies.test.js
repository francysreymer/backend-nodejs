const { equal, deepEqual, ok } = require('assert');
const Sequelize = require('sequelize');
const CompanyModel = require('../src/Model/Company');
const CompaniesController = require('../src/Controller/CompaniesController');
const moment = require('moment');

describe('Companies CRUD', function() {
	this.timeout(Infinity);
	before(async () => {
		//const connection = await CompaniesController.connect()
		//const model = await CompaniesController.defineModel(connection, CompanyModel)
		//console.log('CompanyModel: ', CompanyModel(Sequelize))
		//console.log('CompanyModel model: ', model)
		//console.log('CompanyModel sequelize: ', sequelize)
	

		//await context.create({name:"Test"});
	});

	it.only('MySQL connection', async () => {
		const {Company, sequelize} = await CompanyModel(Sequelize)
		const model = await CompaniesController.defineModel(Company)
		context = new CompaniesController(model, sequelize);
		const result = await context.isConnected();
		equal(result, true);
	});

	it.only('CREATE', async () => {

		const {Company, sequelize} = await CompanyModel(Sequelize)
		const model = await CompaniesController.defineModel(Company)
		context = new CompaniesController(model, sequelize);
	


		const item = {name:"Test Company", created_at:new Date(), active:true};
		console.log('create item: ', item);
		let result = await context.create(item);
		delete result.dataValues.id
		delete result.dataValues.updated_at
		console.log('create item 2: ', item);
		console.log('result.dataValues: ', result.dataValues);
		console.log('moment().format(): ', moment().format());
		deepEqual(result.dataValues, item);
	});

  it('READ', async () => {

	//const {Company, sequelize} = await CompaniesController.defineModel()
	//console.log('CompanyModel sequelize: ', sequelize)
	//context = new CompaniesController(Company, sequelize);

	const {Company, sequelize} = await CompanyModel(Sequelize)
	const model = await CompaniesController.defineModel(Company)
	context = new CompaniesController(model, sequelize);


	const item = {id: 34};
	const [result] = await context.read(item);
	
	console.log('read result.dataValues: ', result.dataValues);

    delete result.dataValues.created;
    delete result.dataValues.modified;

    deepEqual(result.dataValues, item);
  });

  /*it('UPDATE', async () => {
	const item = {name: "Company UPDATED"};
	const id = 16;
    const [result] = await context.update(id, item);
    deepEqual(result, 1);
  });

  it('DELETE', async () => {
    const [item] = await context.read({});
	const [result] = await context.delete(item.id);
	
	console.log('DELETE result: ', result)
    deepEqual(result, 1);
  });*/

});
